# Introduction
hello Beaumont-en-auge ! 
<i class="fas fa-gamepad"></i>
<i class="far fa-campfire"></i>

Auteur : {{ book.author }}

Date de fabrication : {{ gitbook.time }}

Point de départ : [https://gitlab.com/pages/gitbook](https://gitlab.com/pages/gitbook)

## Ebooks

* [Support en formation PDF](/gitbook-publication.pdf)
* [Support en formation EPUB](/gitbook-publication.epub)
* [Support en formation MOBI](/gitbook-publication.mobi)

![Superman S symbol](https://upload.wikimedia.org/wikipedia/commons/0/05/Superman_S_symbol.svg)

Source de l'image : [Superman S symbol](https://commons.wikimedia.org/wiki/File:Superman_S_symbol.svg)
